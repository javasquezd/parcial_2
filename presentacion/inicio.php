<?php
require_once "logica/Estudiante.php";
require_once "logica/Curso.php";
?>
<nav class="navbar navbar-expand-lg navbar-light bg-warning">
  <a class="navbar-brand" href="index.php">Calificaciones Ltda</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Casa<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estudiantes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/insertarEstudiante.php")?>">Insertar estudiante</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/consultarEstudiante.php")?>">Consultar estudiantes</a>
        </div>
	  </li>
	  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cursos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/curso/insertarCurso.php")?>">Insertar curso</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/curso/consultarCurso.php")?>">Consultar curso</a>
        </div>
      </li>
     
    </ul>
    
  </div>
</nav>
