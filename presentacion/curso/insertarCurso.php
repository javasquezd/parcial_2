<?php

?>
<?php
$nombreCurso = "";

if (isset($_POST["nombreCurso"])) {
    $nombreCurso = $_POST["nombreCurso"];
}

$creditos = "";

if (isset($_POST["creditos"])) {
    $creditos = $_POST["creditos"];
}

if (isset($_POST["registrar"])) {
    $nombreCurso = $_POST["nombreCurso"];
    $creditos = $_POST["creditos"];

    $curso = new Curso("", $nombreCurso, $creditos);

    $curso->insertar();
}
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-3 col-md-0"></div>
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header text-center text-black bg-warning">
                    <h4>Insertar curso</h4>
                </div>
                <div class="card-body">
                    <form  method="post">
                        <div class="row">
                            <div class="col-10">
                                <div class="form-group">

                                    <input type="text" name="nombreCurso" class="form-control" placeholder="Nombre" value="<?php echo $nombreCurso ?>" required>
                                </div>
                                <div class="form-group">
                                    <input type="number" name="creditos" class="form-control" placeholder="Creditos"min="1" value="<?php echo $creditos ?>" required>
                                    
                                </div>

                                <button type="submit" name="registrar" class="btn btn-warning btn-block">Insertar</button>
                            </div>


                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>